;;; gcrypt-utils/pk.scm

;; LICENSE HERE

;;; Description:

;; Module adding support to guile-gcrypt for encrypt and decrypt using RSA

(define-module (gcrypt-utils pk)
  #:use-module (gcrypt internal)
  #:use-module (gcrypt pk-crypto)
  #:use-module (gcrypt common)
  #:use-module (gcrypt base16)
  #:use-module (gcrypt base64)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:export (testkey
            encrypt
            decrypt
            pkcs1-encrypt-data
            base64->pkcs1-decrypt-sexp
            pkcs1-decrypt-ciph))

(define canonical-sexp->pointer (@@ (gcrypt pk-crypto) canonical-sexp->pointer))
(define pointer->canonical-sexp (@@ (gcrypt pk-crypto) pointer->canonical-sexp))

(define testkey
  (let ((proc (libgcrypt->procedure int "gcry_pk_testkey" '(*))))
    (lambda (key)
      (let ((res (proc (canonical-sexp->pointer key))))
        (format #t "~a~%" (error-string res))))))

(define encrypt
  (let ((proc (libgcrypt->procedure int "gcry_pk_encrypt" '(* * *))))
    (lambda (data secret-key)
      (let* ((ciph (bytevector->pointer (make-bytevector (sizeof '*))))
             (err (proc ciph (canonical-sexp->pointer data)
                        (canonical-sexp->pointer secret-key))))
        (if (= 0 err)
            (pointer->canonical-sexp (dereference-pointer ciph))
            (throw 'gcry-error 'encrypt err))))))

(define decrypt
  (let ((proc (libgcrypt->procedure int "gcry_pk_decrypt" '(* * *))))
    (lambda (ciph secret-key)
      (let* ((plain (bytevector->pointer (make-bytevector (sizeof '*))))
             (err (proc plain (canonical-sexp->pointer ciph)
                        (canonical-sexp->pointer secret-key))))
        (if (zero? err)
            (pointer->canonical-sexp (dereference-pointer plain))
            (throw 'gcrypt-error 'decrypt err))))))

(define (pkcs1-encrypt-data plain-text)
  "Prepare a sexp with plain-text to be encrypted using PKCS1"
  (let ((data (bytevector->base16-string (string->utf8 plain-text))))
    (string->canonical-sexp (format #f "(data (flags pkcs1) (value #~a#))" data))))

(define (base64->pkcs1-decrypt-sexp b64-data)
  "Prepare a sexp with b64-data to be decrypted using PKCS1"
  (let* ((data (base64-decode b64-data))
         (sexp `(enc-val (flags pkcs1) (rsa (a ,data)))))
    (sexp->canonical-sexp sexp)))

(define (pkcs1-decrypt-ciph ciph)
  (sexp->canonical-sexp
   (match-let ((('enc-val ('rsa ('a c))) (canonical-sexp->sexp ciph)))
     `(enc-val (flags pkcs1) (rsa (a ,c))))))

