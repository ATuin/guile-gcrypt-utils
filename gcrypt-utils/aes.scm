;;; gcrypt-utils/aes.scm

;; LICENSE HERE

;;; Description:

;; Modulo adding support to guile-crypt to use AES

(define-module (gcrypt-utils aes)
  #:use-module (gcrypt internal)
  #:use-module (system foreign)
  #:use-module (srfi srfi-9)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:export (make-aes
            aes-key!
            aes-closed?
            aes-close
            aes-encrypt
            aes-decrypt
            with-aes))

(define cipher-open
  (let ((proc (libgcrypt->procedure int "gcry_cipher_open" `(* ,int ,int ,int))))
    (lambda ()
      (let* ((handler (bytevector->pointer (make-bytevector (sizeof '*))))
             (err (proc handler 7 3 0)))
	(if (= 0 err)
            (dereference-pointer handler)
            (throw 'gcry-error 'cipher-open err))))))

(define cipher-close
  (let ((proc (libgcrypt->procedure void "gcry_cipher_close" '(*))))
    (lambda (handler)
      (proc handler))))

(define cipher-setkey
  (let ((proc (libgcrypt->procedure int "gcry_cipher_setkey" `(* * ,size_t))))
    (lambda (handler key)
      (let ((err (proc handler (bytevector->pointer key) (bytevector-length key))))
	(if (= 0 err) #t (throw 'gcry-error 'cipher-setkey err))))))

(define cipher-setiv
  (let ((proc (libgcrypt->procedure int "gcry_cipher_setiv" `(* * ,size_t))))
    (lambda (handler iv)
      (let* ((err (proc handler (bytevector->pointer iv) (bytevector-length iv))))
	(if (= 0 err) #t (throw 'gcry-error 'cipher-setiv err))))))

(define (bytevector-aes-padding bv)
  (let* ((bv-len (bytevector-length bv))
	 (p (match (remainder bv-len 16)
	      (0 `(,bv-len . ,(+ bv-len 16)))
	      (15 `(,bv-len . ,(+ bv-len 17)))
	      (n `(,bv-len . ,(+ bv-len (- 16 n))))))
	 (bv2 (make-bytevector (cdr p))))
      (bytevector-copy! bv 0 bv2 0 bv-len)
      (let iter ((i (car p)) (j (1- (cdr p))) (n 1))
	(cond
	 ((> i j) bv2)
	 ((= i j)
	  (bytevector-u8-set! bv2 i n)
	  (iter (1+ i) j (1+ n)))
	 (#t
	  (bytevector-u8-set! bv2 i 0)
	  (iter (1+ i) j (1+ n)))))))

(define* (bytevector-aes-depadding bv #:key len)
  (let* ((len (or len (bytevector-length bv)))
	 (n (- len (bytevector-s8-ref bv (1- len))))
	 (bv2 (make-bytevector n)))
    (bytevector-copy! bv 0 bv2 0 n)
    bv2))

;; TODO: Seems to be broken when buf-len is not the same len as data
(define (cipher-loop cipher cipher-proc in len buf-len)
  (let* ((len (or len (bytevector-length in)))
	 (buf-len (or (and buf-len (min buf-len len)) len))
	 (out (make-bytevector len)))
    (let loop ((i 0) (err 0))
      (cond
       ((not (zero? err)) (throw 'gcrypt-error 'cipher-encrypt err))
       ((>= i len) out)
       (#t (loop (+ i buf-len) (cipher-proc cipher (bytevector->pointer out i) buf-len
					    (bytevector->pointer in i) buf-len)))))))

(define cipher-encrypt
  (let ((proc (libgcrypt->procedure int "gcry_cipher_encrypt"
				    `(* * ,size_t * ,size_t))))
    (lambda* (cipher plain-text #:optional len #:key buf-len padding)
      (let* ((in (or (and padding (padding plain-text)) plain-text)))
	(cipher-loop cipher proc in len buf-len)))))

(define cipher-decrypt
  (let ((proc (libgcrypt->procedure int "gcry_cipher_decrypt"
				    `(* * ,size_t * ,size_t))))
    (lambda* (cipher cipher-text #:optional len #:key buf-len padding)
      (cipher-loop cipher proc cipher-text len buf-len))))

(define-record-type <aes>
  (new-aes cipher key closed?)
  aes?
  (cipher _aes-cipher aes-cipher!)
  (key aes-key aes-key!)
  (closed? aes-closed?))

(define (aes-cipher aes)
  (when (aes-closed? aes)
    (aes-cipher! aes (cipher-open)))
  (_aes-cipher aes))

(define (aes-op op aes data iv)
 (let ((c (aes-cipher aes)))
   (cipher-setkey c (aes-key aes))
   (cipher-setiv c iv)
   (match op
     ('encrypt (cipher-encrypt c (bytevector-aes-padding data)
			       #:buf-len (bytevector-length data)))
     ('decrypt (cipher-decrypt c data #:buf-len (bytevector-length data)))
     (#t (error "Unknown AES operation!")))))

(define (aes-encrypt aes data iv)
  (aes-op 'encrypt aes data iv))

(define* (aes-decrypt aes data #:optional iv #:key (remove-pad #t))
  (let ((bv (aes-op 'decrypt aes data (or iv (make-bytevector 16 0)))))
    (if remove-pad (bytevector-aes-depadding bv) bv)))

(define (aes-close aes)
  (if (aes-closed? aes) #t (cipher-close (_aes-cipher aes))))

(define (make-aes key)
  (new-aes (cipher-open) key #f))

(define-syntax with-aes
  (syntax-rules ()
    ((with-aes (c key) exp exp* ...)
     (let ((c (make-aes key)))
       (let ((r (begin exp exp* ...)))
	 (aes-close c)
	 r)))))
